import { Injectable } from '@angular/core'
import { Storage } from '@ionic/storage';
import { Headers, RequestOptions } from '@angular/http'

@Injectable()
export class DataService {

  public headers
  public APIKeyGoogle     = 'AIzaSyB_z2DgWWo8o6AugQAmn6fjX6ymj9-3j3o'

  // ---- URL for RESTful API -----

  public baseUrl = 'https://iosapi.mytaxguide.id/'
  public url      = []
  public detail   = '-detail'
  public image    = '-image'
  public search   = '-search'
  public comment  = '-comment'
  public related  = '-related'
  public distance = '-distance'
  public login    = '-login'
  public social   = '-login_social_media'
  public register = '-registration'
  public month    = '-month'
  public day      = '-day'
  public daySeminar     = '-daySeminar'
  public forgotPassword = '-forgot_password'
  public getDataProfile = '-get_data_profile'
  public changePassword = '-change_password'
  public editProfile = '-edit_profile'
  public updateProfilePicture = '-update_profile_picture'

  // Indonesian Tax Guide Const
  public indonesianTaxGuideCategory       = ['tax-rates', 'tax-treaty', 'tax-time-test', 'map-code', 'kurs-pajak']
  public indonesianTaxGuideCategoryConst  = [1, 2, 3, 4, 5]

  // Indonesian Learning Tax Const
  public indonesianLearningTaxCategory      = ['infografik', 'video']
  public indonesianLearningTaxCategoryConst = [1, 2]

  public origins = '&origins='
  public destinations = '&destinations='

  private countLimit = 10
  private countLimitRelated = 5

  public withLimiterAndPage = '&limit='+ this.countLimit +'&page='
  public withLimiterAndPageRelated = '&limit='+ this.countLimitRelated
  public specific_header_ios: any

  // constructor
  constructor(
    public storage: Storage
  ) {
    this.setURL()
    this.setHeader()
    this.specific_header_ios = 'Token=' + localStorage.getItem('token') + '&Token-Login=' + localStorage.getItem('login_token')
  }

  setHeader(){
    
    let hdrs = new Headers({
      'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8'
    });
    
    let options = new RequestOptions({ headers: hdrs });
    return options
  }

  setTokenHeader(){
    this.headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.headers
  }


  private setURL(){
    this.specific_header_ios = 'Token=' + localStorage.getItem('token') + '&Token-Login=' + localStorage.getItem('login_token')
    
    // get Token
    this.url['token']                   = this.baseUrl + "token/create_token?" + this.specific_header_ios

    // tax office location
    this.url['tax-office-location']     = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=kantor-pajak&radius=10000&key=" + this.APIKeyGoogle + "&location="

    this.url['tax-office-location' + this.distance] = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&key=" + this.APIKeyGoogle

    // contact us
    this.url['contact-us']              = this.baseUrl + 'contact_us/post_contact_us?' + this.specific_header_ios

    // comment
    this.url['comment']                 = this.baseUrl + 'comment/input_comment?' + this.specific_header_ios

    // login and register
    this.url['login-register' + this.login]           = this.baseUrl + 'member/login?' + this.specific_header_ios
    this.url['login-register' + this.social]          = this.baseUrl + 'member/login_social_media?' + this.specific_header_ios
    this.url['login-register' + this.register]        = this.baseUrl + 'member/registration?' + this.specific_header_ios
    this.url['login-register' + this.getDataProfile]  = this.baseUrl + 'member/get_data_profile?' + this.specific_header_ios
    this.url['login-register' + this.forgotPassword]  = this.baseUrl + 'member/reset_password?' + this.specific_header_ios
    this.url['login-register' + this.changePassword]  = this.baseUrl + 'member/change_password?' + this.specific_header_ios
    this.url['login-register' + this.editProfile]     = this.baseUrl + 'member/edit_profile?' + this.specific_header_ios
    this.url['login-register' + this.updateProfilePicture] = this.baseUrl + 'member/update_profile_picture?' + this.specific_header_ios

    // slide
    this.url['slide']                   = this.baseUrl + 'article/get_slider?' + this.specific_header_ios

    // news
    this.url['news']                    = this.baseUrl + 'article/get_list_news?' + this.specific_header_ios + '&limit=' + this.countLimit + '&page='
    this.url['news' + this.search]      = this.baseUrl + 'article/get_search_result_news?' + this.specific_header_ios + '&search='

    this.url['news' + this.detail]      = this.baseUrl + 'article/get_detail_news?'+ this.specific_header_ios + '&item_id='
    this.url['news' + this.image]       = this.baseUrl + 'article/get_detail_news_images?'+ this.specific_header_ios + '&item_id='
    this.url['news' + this.related]     = this.baseUrl + 'article/get_related_news?'+ this.specific_header_ios + '&item_id='
    this.url['news' + this.comment]     = this.baseUrl + 'article/get_comment_news?'+ this.specific_header_ios + '&item_id='

    // article
    this.url['article']                 = this.baseUrl + 'article/get_list_article?'+ this.specific_header_ios + '&limit=' + this.countLimit + '&page='
    this.url['article' + this.search]   = this.baseUrl + 'article/get_search_result_article?'+ this.specific_header_ios + '&search='

    this.url['article' + this.detail]   = this.baseUrl + 'article/get_detail_article?'+ this.specific_header_ios + '&item_id='
    this.url['article' + this.image]    = this.baseUrl + 'article/get_detail_article_images?'+ this.specific_header_ios + '&item_id='
    this.url['article' + this.related]  = this.baseUrl + 'article/get_related_article?'+ this.specific_header_ios + '&item_id='
    this.url['article' + this.comment]  = this.baseUrl + 'article/get_comment_article?'+ this.specific_header_ios + '&item_id='

    // seminar
    this.url['seminar']                 = this.baseUrl + 'seminar/get_list_seminar?'+ this.specific_header_ios + '&limit=' + 365 + '&page='
    this.url['seminar' + this.search]   = this.baseUrl + 'seminar/get_search_result_seminar?'+ this.specific_header_ios + '&search='

    this.url['seminar' + this.detail]     = this.baseUrl + 'seminar/get_detail_seminar?'+ this.specific_header_ios + '&item_id='
    this.url['seminar' + this.image]      = this.baseUrl + 'seminar/get_detail_seminar_images?'+ this.specific_header_ios + '&item_id='
    this.url['seminar' + this.related]    = this.baseUrl + 'seminar/get_related_seminar?'+ this.specific_header_ios + '&item_id='
    this.url['seminar' + this.comment]    = this.baseUrl + 'seminar/get_comment_seminar?'+ this.specific_header_ios + '&item_id='
    this.url['seminar' + this.month]      = this.baseUrl + 'seminar/get_list_seminar_bymonth?' + this.specific_header_ios
    this.url['seminar' + this.day]        = this.baseUrl + 'seminar/get_list_seminar_bydate?' + this.specific_header_ios
    this.url['seminar' + this.daySeminar] = this.baseUrl + 'seminar/get_list_seminar_bydateseminar?' + this.specific_header_ios
    this.url['seminar' + this.register]   = this.baseUrl + 'seminar/input_pendaftaran_seminar?' + this.specific_header_ios

    // tax books
    this.url['tax-books']                 = this.baseUrl + 'tax_books/get_list_tax_books?'+ this.specific_header_ios + '&limit=' + this.countLimit + '&page='
    this.url['tax-books' + this.search]   = this.baseUrl + 'tax_books/get_search_result_tax_books?'+ this.specific_header_ios + '&search='

    this.url['tax-books' + this.detail]   = this.baseUrl + 'tax_books/get_detail_tax_books?'+ this.specific_header_ios + '&item_id='
    this.url['tax-books' + this.image]    = this.baseUrl + 'tax_books/get_detail_tax_books_images?'+ this.specific_header_ios + '&item_id='
    this.url['tax-books' + this.related]  = this.baseUrl + 'tax_books/get_related_tax_books?'+ this.specific_header_ios + '&item_id='
    this.url['tax-books' + this.comment]  = this.baseUrl + 'tax_books/get_comment_tax_books?'+ this.specific_header_ios + '&item_id='

    //tax custom regulation
    this.url['kurs-pajak-level']               = this.baseUrl + 'tax_custom_regulation/get_list_level?' + this.specific_header_ios
    this.url['kurs-pajak-topik']               = this.baseUrl + 'tax_custom_regulation/get_list_topik?'+ this.specific_header_ios
    this.url['kurs-pajak-searchTaxRegulation'] = this.baseUrl + 'tax_custom_regulation/get_search_result_tax_custom_regulation?'+ this.specific_header_ios
    this.url['kurs-pajak-detailTaxRegulation'] = this.baseUrl + 'tax_custom_regulation/get_detail_tax_custom_regulation?'+ this.specific_header_ios + '&item_id='

    // Indonesian Tax Guide Category
    for (let i = 0; i < this.indonesianTaxGuideCategory.length; i++) {
      this.url[this.indonesianTaxGuideCategory[i]]                 = this.baseUrl + 'indonesia_tax_guide/get_list_indonesia_tax_guide?'+ this.specific_header_ios + '&limit=' + this.countLimit + '&category_id='+ this.indonesianTaxGuideCategoryConst[i] +'&page='
      this.url[this.indonesianTaxGuideCategory[i] + this.search]   = this.baseUrl + 'indonesia_tax_guide/get_search_result_indonesia_tax_guide?'+ this.specific_header_ios + '&search='

      this.url[this.indonesianTaxGuideCategory[i] + this.detail]   = this.baseUrl + 'indonesia_tax_guide/get_detail_indonesia_tax_guide?'+ this.specific_header_ios + '&item_id='
      this.url[this.indonesianTaxGuideCategory[i] + this.image]    = this.baseUrl + 'indonesia_tax_guide/get_detail_indonesia_tax_guide_images?'+ this.specific_header_ios + '&item_id='
      this.url[this.indonesianTaxGuideCategory[i] + this.related]  = this.baseUrl + 'indonesia_tax_guide/get_related_indonesia_tax_guide?'+ this.specific_header_ios + '&item_id='
      this.url[this.indonesianTaxGuideCategory[i] + this.comment]  = this.baseUrl + 'indonesia_tax_guide/get_comment_indonesia_tax_guide?'+ this.specific_header_ios + '&item_id='
    }

    // Indonesian Learning Tax
    for (let i = 0; i < this.indonesianLearningTaxCategory.length; i++) {
      this.url[this.indonesianLearningTaxCategory[i]]                 = this.baseUrl + 'learning_tax/get_list_learning_tax?'+ this.specific_header_ios + '&limit=' + this.countLimit + '&category_id='+ this.indonesianLearningTaxCategoryConst[i] +'&page='
      this.url[this.indonesianLearningTaxCategory[i] + this.search]   = this.baseUrl + 'learning_tax/get_search_result_learning_tax?'+ this.specific_header_ios + '&search='

      this.url[this.indonesianLearningTaxCategory[i] + this.detail]   = this.baseUrl + 'learning_tax/get_detail_learning_tax?'+ this.specific_header_ios + '&item_id='
      this.url[this.indonesianLearningTaxCategory[i] + this.image]    = this.baseUrl + 'learning_tax/get_detail_learning_tax_images?'+ this.specific_header_ios + '&item_id='
      this.url[this.indonesianLearningTaxCategory[i] + this.related]  = this.baseUrl + 'learning_tax/get_related_learning_tax?'+ this.specific_header_ios + '&item_id='
      this.url[this.indonesianLearningTaxCategory[i] + this.comment]  = this.baseUrl + 'learning_tax/get_comment_learning_tax?'+ this.specific_header_ios + '&item_id='
    }

    console.log(this.url)

  }

  arrayToQueryString(array_in){
    let out = new Array();
    for(let key in array_in){
        out.push(key + '=' + encodeURIComponent(array_in[key]));
    }
    return out.join('&');
  }
}
