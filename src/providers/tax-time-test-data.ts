import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class TaxTimeTestData extends BaseData{
	nameClass = 'tax-time-test';
}