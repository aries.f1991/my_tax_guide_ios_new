import { Injectable } from '@angular/core'
// import { Http } from '@angular/http'
// import { Storage } from '@ionic/storage';

import { BaseData } from './base-data'

@Injectable()
export class ArticleData extends BaseData{
	nameClass = 'article';
}