import { Injectable } from '@angular/core'
import { BaseData } from './base-data'
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/timeout'

@Injectable()
export class ContactUsData extends BaseData {

  public nameClass = 'contact-us'

}