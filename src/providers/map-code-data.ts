import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class MapCodeData extends BaseData{
	nameClass = 'map-code';
}