import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class KursPajakData extends BaseData{
	nameClass = 'kurs-pajak';
}