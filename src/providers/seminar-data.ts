import { Injectable } from '@angular/core'
import { BaseData } from './base-data'

@Injectable()
export class SeminarData extends BaseData{
	nameClass = 'seminar';
}